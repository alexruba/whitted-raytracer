#include "Utils.h"
#include "Scene.h"

class Ray{

public:
	Ray(Vector orig, Vector dir);
	Ray();

	// Color of the object intersected
	Vector Cast(Scene &scene, int rebounds, int objectMask);
	bool Intersects(Scene &scene, float &tNear, int &index, int &triangleId, SceneObject **hitObject, int mask);
	Vector Phong(Scene &scene, Vector hitPoint, SceneObject *hitObject, int& index, int &triangleId, int rebounds);

	Ray Refract(float distance, Vector normal, float eta);

private:
	Vector origin;
	Vector direction;
};

