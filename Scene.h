/*
  15-462 Computer Graphics I
  Assignment 3: Ray Tracer
  C++ Scene Descriptor Class
  Author: rtark
  Oct 2007

  NOTE: You do not need to edit this file for this assignment but may do so

  This file defines the following:
	SceneObjectType Enumeration
	SceneBackground Class
	SceneLight Class
	SceneMaterial Class
	SceneObject Class -> SceneSphere, SceneTriangle, SceneModel
	Scene Class

  Scene Usage: Several Functions
	Scene::Load (sceneFile) - load a scene descriptor file

	Scene::GetDescription () - get the scene description string
	Scene::GetAuthor () - get the scene author string
	Scene::GetBackground () - get the scene background information
	Scene::GetNumLights () - get the number of lights in the scene
	Scene::GetLight (lightIndex) - get one of the lights in the scene
	Scene::GetNumMaterials () - get the number of materials in the scene
	Scene::GetMaterial (materialIndex OR materialName) - get a material's description
	Scene::GetNumObjects () - get the number of objects in the scene
	Scene::GetObject (objectIndex) - get an object's description
	Scene::GetCamera - get the current camera description

	The trickiest one of these is the GetObject () function
	It is used like this:
	[The object is type-casted to its corresponding type]

	SceneObject tempObject = m_Scene.GetObject (0);
	if (tempObject.IsTriangle ())
	{
		Vector vertices[3];
		
		for (int n = 0; n < 3; n++)
		{
			vertices[n] = ((SceneTriangle)tempObject).vertex[n];
		}
	}
*/

#ifndef __SCENE_H__
#define __SCENE_H__

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <fstream>
#include <iostream>

#include "Utils.h"
#include "pic.h"
#include "glm\glm\glm.hpp"
#include "glm\glm\gtc\matrix_transform.hpp"

// XML Parser by Frank Vanden Berghen
// Available: http://iridia.ulb.ac.be/~fvandenb/tools/xmlParser.html
#include "xmlParser.h"

// 3DS File by bkenwright@xbdev.net
//    Updated by Raphael Mun
#include "3ds.h"

// Max Line Length for OBJ File Loading
#define MAX_LINE_LEN 1000

#define CHECK_ATTR(a) (a == NULL ? "" : a)
#define CHECK_ATTR2(a,b) (a == NULL ? b : a)

/*
	SceneObjectType Namespace - Holds the ObjectType Enumeration
*/
namespace SceneObjectType
{
	enum ObjectType
	{
		Sphere = 0,
		Triangle = 1,
		Model = 2,
	};
};

/*
	SceneBackground Class - The Background properties of a ray-trace scene

	This class defines the background in the Scene
*/
class SceneBackground
{
public:
	Vector color;
	Vector ambientLight;
};

/*
	SceneLight Class - The light properties of a single light-source in a ray-trace scene

	The Scene class holds a list of these
*/
class SceneLight
{
public:
	float attenuationConstant, attenuationLinear, attenuationQuadratic;
	Vector color;
	Vector position;
};

/*
	SceneMaterial Class - The material properties used in a ray-trace scene

	The Scene class holds a list of material
*/
class SceneMaterial
{
	Pic *tex;
public:
	std::string name;
	std::string texture;
	Vector diffuse;
	Vector specular;
	float shininess;
	Vector transparent;
	Vector reflective;
	Vector refraction_index;

	// -- Constructors & Destructors --
	SceneMaterial (void) : tex (NULL)
	{}

	~SceneMaterial (void)
	{
		if (tex)
		{
			pic_free (tex);
		}
	}

	// -- Utility Functions --
	// - LoadTexture - Loads a Texture from a filename
	bool LoadTexture (void)
	{
		tex = jpeg_read ((char *)texture.c_str (), NULL);
		if (tex == NULL)
			return false;

		return true;
	}

	// -- Accessor Functions --
	// - GetTextureColor - Returns the texture color at coordinates (u,v)
	Vector GetTextureColor (float u, float v)
	{
		if (tex)
		{
			int textureX = (int)(u * tex->nx);
			int textureY = (int)(v * tex->ny);

			return Vector (PIC_PIXEL(tex, textureX, textureY, 0),
						PIC_PIXEL(tex, textureX, textureY, 1),
						PIC_PIXEL(tex, textureX, textureY, 2));
		}

		return Vector (1.0f, 1.0f, 1.0f);
	}
};

/*
	SceneObject Class - A base object class that defines the common features of all objects

	This is the base object class that the various scene object types derive from
*/
class SceneObject
{
public:
	std::string name;
	SceneObjectType::ObjectType type;
	Vector scale, rotation, position;

	// -- Constructors & Destructors --
	SceneObject (void) { scale.x = 1.0f; scale.y = 1.0f; scale.z = 1.0f; }
	SceneObject (SceneObjectType::ObjectType tp) : type (tp) { scale.x = 1.0f; scale.y = 1.0f; scale.z = 1.0f; }
	SceneObject (std::string nm, SceneObjectType::ObjectType tp) : name(nm), type (tp) { scale.x = 1.0f; scale.y = 1.0f; scale.z = 1.0f; }

	// -- Object Type Checking Functions --
	bool IsSphere (void) { return (type == SceneObjectType::Sphere); }
	bool IsTriangle (void) { return (type == SceneObjectType::Triangle); }
	bool IsModel (void) { return (type == SceneObjectType::Model); }

	virtual void setValues(Vector &, const Vector &, int, Vector &) const = 0;
	virtual bool intersect(Vector &, Vector &, float &, int &) const = 0;
};

/*
	SceneSphere Class - The sphere scene object

	Sphere object derived from the SceneObject
*/
class SceneSphere : public SceneObject
{
public:
	std::string material;
	Vector center;
	float radius;

	// -- Constructors & Destructors --
	SceneSphere (void) : SceneObject ("Sphere", SceneObjectType::Sphere) {}
	SceneSphere (std::string nm) : SceneObject (nm, SceneObjectType::Sphere) {}

	void setValues(Vector &P, const Vector &I, int triangleId, Vector &N) const
	{
		N = (P - center + position).Normalize();
	}

	bool intersect(Vector &orig, Vector &dir, float &tnear, int &index) const
	{
		Vector L = Vector(center.x + position.x - orig.x, center.y + position.y - orig.y, center.z + position.z - orig.z);
		float distance = L.Dot(L) - pow(radius * (scale.x + scale.y + scale.z) / 3, 2);

		if (distance > 0)
		{
			float p = L.Dot(dir);
			if (p >= 0)
			{
				if (distance == pow(p, 2))
				{	
					tnear = p;
					return true;
				}
				else if (distance < pow(p, 2))
				{
					float s = sqrt(pow(p, 2) - distance);
					tnear = glm::min(p - s, p + s);
					return true;
				}
			}
		}
		return false;
	}
};

/*
	SceneTriangle Class - The triangle scene object

	Single triangle object derived from the SceneObject
*/
class SceneTriangle : public SceneObject
{
public:
	std::string material[3];
	Vector vertex[3];
	Vector normal[3];
	float u[3], v[3];

	// -- Constructors & Destructors --
	SceneTriangle (void) : SceneObject ("Triangle", SceneObjectType::Triangle) {}
	SceneTriangle (std::string nm) : SceneObject (nm, SceneObjectType::Triangle) {}
	Vector toWorldVertex(int indice) const;

	// Compute barycentric coordinates (u, v, w) for
	// point p with respect to triangle (a, b, c)
	// http://gamedev.stackexchange.com/a/23745
	void Barycentric(Vector p, Vector &barycentricCoords)
	{
		Vector v0 = vertex[1] - vertex[0], v1 = vertex[2] - vertex[0], v2 = p - vertex[0];
		float d00 = v0.Dot(v0);
		float d01 = v0.Dot(v1);
		float d11 = v1.Dot(v1);
		float d20 = v2.Dot(v0);
		float d21 = v2.Dot(v1);
		float denom = d00 * d11 - d01 * d01;

		barycentricCoords.x = (d11 * d20 - d01 * d21) / denom;
		barycentricCoords.y = (d00 * d21 - d01 * d20) / denom;
		barycentricCoords.z = 1.0f - barycentricCoords.y - barycentricCoords.x;
	}

	void setValues(Vector &P, const Vector &I, int triangleId, Vector &N) const
	{
		const Vector &v0 = toWorldVertex(0);
		const Vector &v1 = toWorldVertex(1);
		const Vector &v2 = toWorldVertex(2);

		Vector e0 = Vector(v1.x - v0.x, v1.y - v0.y, v1.z - v0.z);
		Vector e1 = Vector(v2.x - v1.x, v2.y - v1.y, v2.z - v1.z);

		N = e0.Cross(e1).Normalize();
	}

	// M�ller-Trumbore intersection Algorithm
	bool intersect(Vector &orig, Vector &dir, float &tnear, int &index) const
	{
		Vector e0, e1, pvec, qvec, tvec;
		float det, inv_det, u, v;
		float phi = 0.00001;

		Vector v0 = toWorldVertex(0);
		Vector v1 = toWorldVertex(1);
		Vector v2 = toWorldVertex(2);

		e0 = v1 - v0;
		e1 = v2 - v0;

		pvec = dir.Cross(e1);

		det = e0.Dot(pvec);

		if ((det>-phi) && (det<phi)) return false;

		inv_det = 1 / det;
		tvec = orig - v0;
		u = (tvec.Dot(pvec))*inv_det;

		if (u<0.0f || u>1.0f) return false;

		qvec = tvec.Cross(e0);
		v = (dir.Dot(qvec))*inv_det;

		if ((v<0.0f) || ((u + v)>1.0f)) return false;

		float t = (e1.Dot(qvec))*inv_det;

		if (t > phi){
			tnear = t;
			return true;
		}

		return false;
	}
};

/*
	SceneModel Class - The model scene object

	A model object consisting of a list of triangles derived from the SceneObject
*/
class SceneModel : public SceneObject
{
public:
	std::string filename;
	std::vector<SceneTriangle> triangleList;

	// -- Constructors & Destructors --
	SceneModel (void) : SceneObject ("Model", SceneObjectType::Model) {}
	SceneModel (std::string file) : SceneObject ("Model", SceneObjectType::Model) { filename = file; }
	SceneModel (std::string file, std::string nm) : SceneObject (nm, SceneObjectType::Model) { filename = file; }

	// -- Accessor Functions --
	// - GetNumTriangles - Returns the number of triangles in the model
	unsigned int GetNumTriangles (void) { return (unsigned int)triangleList.size (); }

	// - GetTriangle - Gets the nth SceneTriangle
	SceneTriangle *GetTriangle (int triIndex) { return &triangleList[triIndex]; }

	void setValues(Vector &P, const Vector &I, int triangleId, Vector &N) const
	{
			SceneTriangle tri = triangleList[triangleId];
			tri.setValues(P, I, triangleId, N);
	}

	bool intersect(Vector &orig, Vector &dir, float &tnear, int &index) const
	{
		bool intersect = false;
		float tAux = std::numeric_limits<float>::infinity();

		for (int k = 0; k < (unsigned int)triangleList.size(); ++k)
		{
			float t;
			SceneTriangle tri = triangleList[k];

			if (tri.intersect(orig, dir, t, k) && t > 0 && t < tAux)
			{
				tAux = t;
				tnear = t;
				index = k;
				intersect = true;
			}
		}

		return intersect;
	}
};

/*
	Scene Class - The main scene definition class with the scene information

	This is the base scene class
*/
class Scene
{
public:
   static const int WINDOW_HEIGHT, WINDOW_WIDTH;
   static bool supersample;
   static bool montecarlo;

private:
	std::string m_Desc, m_Author;
	SceneBackground m_Background;
	std::vector<SceneLight *> m_LightList;
	std::vector<SceneMaterial *> m_MaterialList;
	std::vector<SceneObject *> m_ObjectList;

	// - Private utility Functions used by Load () -
	Vector ParseColor (XMLNode node)
	{
		if (node.isEmpty ())
			return Vector (0.0f, 0.0f, 0.0f);
		return Vector (atof(node.getAttribute("red")), 
						atof(node.getAttribute("green")),
						atof(node.getAttribute("blue")));
	}

	Vector ParseXYZ (XMLNode node)
	{
		if (node.isEmpty ())
			return Vector (0.0f, 0.0f, 0.0f);
		return Vector (atof(node.getAttribute("x")), 
					   atof(node.getAttribute("y")),
					   atof(node.getAttribute("z")));
	}

	void ParseOBJCommand (char *line, int max, char *command, int &position);
	Vector ParseOBJVector (char *str);
	bool ParseOBJCoords (char *str, int &num, int v_index[3], int n_index[3]);
public:
	Camera m_Camera;


	// -- Constructors & Destructors --
	Scene (void) {}
	~Scene (void)
	{
		// Free the memory allocated from the objects
		unsigned int numObj = GetNumObjects ();
		for (unsigned int n = 0; n < numObj; n++)
		{
			SceneObject *sceneObj = m_ObjectList[n];
			if (sceneObj->IsSphere ())
			{
				delete ((SceneSphere *)sceneObj);
			}
			else if (sceneObj->IsTriangle ())
			{
				delete ((SceneTriangle *)sceneObj);
			}
			else if (sceneObj->IsModel ())
			{
				delete ((SceneModel *)sceneObj);
			}
		}
		m_ObjectList.clear ();
      // Free the memory allocated for the materials
      unsigned int numMat = GetNumMaterials ();
		for (unsigned int n = 0; n < numMat; n++)
		{
		   delete m_MaterialList[n];
		}
		m_MaterialList.clear ();
      // Free the memory allocated for the lights
      unsigned int numLight = GetNumLights ();
		for (unsigned int n = 0; n < numLight; n++)
		{
		   delete m_LightList[n];
		}
		m_LightList.clear ();
	}

	// -- Main Functions --
	// - Load - Loads the Scene XML file
	bool Load (char *filename);

	// -- Accessor Functions --
	// - GetDescription - Returns the Description String
	const char * GetDescription (void) { return m_Desc.c_str(); }

	// - GetAuthor - Return the Author String
	const char * GetAuthor (void) { return m_Author.c_str(); }

	// - GetBackground - Returns the SceneBackground
	const SceneBackground& GetBackground (void) const { return m_Background; }

	// - GetNumLights - Returns the number of lights in the scene
	unsigned int GetNumLights (void) { return (unsigned int)m_LightList.size (); }

	// - GetLight - Returns the nth SceneLight
	SceneLight* GetLight (int lightIndex) const { return m_LightList[lightIndex]; }

	// - GetNumMaterials - Returns the number of materials in the scene
	unsigned int GetNumMaterials (void) { return (unsigned int)m_MaterialList.size (); }

	// - GetMaterial - Returns the nth SceneMaterial
	SceneMaterial* GetMaterial (int matIndex) const { return m_MaterialList[matIndex]; }
	SceneMaterial* GetMaterial (std::string matName) const
	{
		unsigned int numMats = (unsigned int)m_MaterialList.size ();
		for (unsigned int n = 0; n < numMats; n++)
		{
			if (matName == m_MaterialList[n]->name)
				return m_MaterialList[n];
		}

		return NULL;
	}

	// - GetNumObjects - Returns the number of objects in the scene
	unsigned int GetNumObjects (void) { return (unsigned int)m_ObjectList.size (); }

	// - GetObject - Returns the nth object [NOTE: The Object will need to be type-casted afterwards]
	SceneObject *GetObject (int objIndex) { return m_ObjectList[objIndex]; }

	// - GetCamera - Returns the camera class
	Camera GetCamera (void) { return m_Camera; }
};



#endif
