#ifdef _OS_X_
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#include <GLUT/glut.h>  

#elif defined(WIN32)
#include <windows.h>
#include "GL/gl.h"
#include "GL/glu.h"
#include "GL/glut.h"

#else
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#endif

#include <iostream>
#include <limits>

#include "Scene.h"
#include "RayTrace.h"
#include "Ray.h"

const float fInfinity = std::numeric_limits<float>::infinity();

// -- Main Functions --
// - CalculatePixel - Returns the Computed Pixel for that screen coordinate
Vector RayTrace::CalculatePixel (int screenX, int screenY)
{
   /*
   -- How to Implement a Ray Tracer --

   This computed pixel will take into account the camera and the scene
   and return a Vector of <Red, Green, Blue>, each component ranging from 0.0 to 1.0

   In order to start working on computing the color of this pixel,
   you will need to cast a ray from your current camera position
   to the image-plane at the given screenX and screenY
   coordinates and figure out how/where this ray intersects with 
   the objects in the scene descriptor.
   The Scene Class exposes all of the scene's variables for you 
   through its functions such as m_Scene.GetBackground (), m_Scene.GetNumLights (), 
   etc. so you will need to use those to learn about the World.

   To determine if your ray intersects with an object in the scene, 
   you must calculate where your objects are in 3D-space [using 
   the object's scale, rotation, and position is extra credit]
   and mathematically solving for an intersection point corresponding to that object.

   For example, for each of the spheres in the scene, you can use 
   the equation of a sphere/ellipsoid to determine whether or not 
   your ray from the camera's position to the screen-pixel intersects 
   with the object, then from the incident angle to the normal at 
   the contact, you can determine the reflected ray, and recursively 
   repeat this process capped by a number of iterations (e.g. 10).

   Using the lighting equation & texture to calculate the color at every 
   intersection point and adding its fractional amount (determined by the material)
   will get you a final color that returns to the eye at this point.
   */

   Scene &la_escena = m_Scene;
   Camera &la_camara = la_escena.GetCamera();
   Vector posicion = la_camara.GetPosition();

   if ((screenX < 0 || screenX > Scene::WINDOW_WIDTH - 1) ||
      (screenY < 0 || screenY > Scene::WINDOW_HEIGHT - 1))
   {
      // Off the screen, return black
      return Vector (0.0f, 0.0f, 0.0f);
   }
   Vector orig = la_camara.GetPosition();
   int R = 4; // Rebotes

   float aspectRatio = Scene::WINDOW_WIDTH / (float)Scene::WINDOW_HEIGHT;
   float fovValue = tan(la_camara.GetFOV() / 2 * 3.141592653589793 / 180);
   float widthBy2 = (float)Scene::WINDOW_WIDTH / 2.0f;
   float heightBy2 = (float)Scene::WINDOW_HEIGHT / 2.0f;

   Vector lookAt = la_camara.GetTarget() - orig;
   lookAt = lookAt.Normalize();
   Vector N = la_camara.GetUp().Cross(lookAt);
   N = N.Normalize();
   Vector right = la_camara.GetUp().Cross(lookAt);
   right = right.Normalize();

   // Rotated grid supersampling sample pattern
   // https://en.wikipedia.org/wiki/Supersampling
   // Hector Barreiro and Rosa Sanchez sample pattern values
   if (m_Scene.supersample)
   {
      float sqrt5Over2 = 1.11803398875f;

      float samplePattern[4][2] = {
         { 0.111803f * sqrt5Over2, 0.33541f * sqrt5Over2 },
         { -0.33541f * sqrt5Over2, 0.111803f * sqrt5Over2 },
         { 0.33541f * sqrt5Over2, -0.111803f * sqrt5Over2 },
         { -0.111803f * sqrt5Over2, -0.33541f * sqrt5Over2 }
      };

      Vector color;

      for (int i = 0; i < 4; i++)
      {
         Vector dir = lookAt + N * fovValue * aspectRatio * ((widthBy2 - screenX) / widthBy2  + 1.0f / widthBy2 * samplePattern[i][0]) +
            la_camara.GetUp() * fovValue * ((screenY - heightBy2) / heightBy2 + 1.0f / heightBy2 * samplePattern[i][1]);
         dir = dir.Normalize();

         // Lanzamos un rayo en la escena hasta un m�ximo de R rebotes
         Ray ray(orig, dir);
         color = color + ray.Cast(m_Scene, R, -1);
      }

      return color * (1.0f / 4.0f);
   }
   else
   {
      Vector dir = lookAt + N * fovValue * aspectRatio * (widthBy2 - screenX) / widthBy2 +
         la_camara.GetUp() * fovValue * (screenY - heightBy2) / heightBy2;
      dir = dir.Normalize();

      Ray ray(orig, dir);

      // Lanzamos un rayo en la escena hasta un m�ximo de R rebotes
      return ray.Cast(m_Scene, R, -1);
   }
}