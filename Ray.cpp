#include "Ray.h"
#include <iostream>
#include <stdlib.h>

using namespace std;

const float fInfinity = std::numeric_limits<float>::infinity();

Vector clamp(Vector v)
{
	v.x = glm::clamp(v.x, 0.0f, 1.0f);
	v.y = glm::clamp(v.y, 0.0f, 1.0f);
	v.z = glm::clamp(v.z, 0.0f, 1.0f);

	return v;
}

Ray::Ray()
{
	this->origin = Vector(0.0f, 0.0f, 0.0f);
	this->direction = Vector(0.0f, 0.0f, 0.0f);
}

Ray::Ray(Vector orig, Vector dir)
{
	this->origin = orig;
	this->direction = dir;
}


Ray Ray::Refract(float distance, Vector normal, float eta)
{
	// https://www.opengl.org/sdk/docs/man/html/refract.xhtml

	float NdotI = direction.Dot(normal);
	float k = 1.0f - eta * eta * (1.0f - NdotI * NdotI);

	Ray refractedRay;
	refractedRay.direction = (k < 0.0f) ? Vector() : (direction * eta - normal * (eta * NdotI + sqrtf(k))).Normalize();
	refractedRay.origin = origin + direction * distance + refractedRay.direction * 1e-3f;

	return refractedRay;
}

Vector Ray::Cast(Scene &scene, int rebounds, int objectMask)
{
	// Color por defecto si no hay intersecci�n
	Vector hitColor = scene.GetBackground().color;
	float nearest = fInfinity;
	int index = -1;
	int triangleId = -1;
	SceneObject *hitObject = nullptr;

	// Comprobar si hay intersecci�n
	if (Intersects(scene, nearest, index, triangleId, &hitObject, objectMask))
	{
		Vector hitPoint = origin + direction * nearest;

		// Calculamos color en esta posici�n
		hitColor = Phong(scene, hitPoint, hitObject, index, triangleId, rebounds);
	}

	return hitColor;
		
}

bool Ray::Intersects(Scene &scene, float &nearest, int &index, int &triangleId, SceneObject **hitObject, int objectMask)
{
	bool intersect = false;
	*hitObject = nullptr;

	// Comprobar la intersecci�n con todos los objetos de la escena
	for (int k = 0; k < scene.GetNumObjects(); ++k)
	{
		float nearestAux;
		int triangleIdAux;

		// Sin tener en cuenta un objeto
		if (objectMask != k)
		{
			if (scene.GetObject(k)->intersect(origin, direction, nearestAux, triangleIdAux) && nearestAux > 0 && nearestAux < nearest)
			{
				*hitObject = scene.GetObject(k);
				nearest = nearestAux;
				index = k;
				triangleId = triangleIdAux;
				intersect = true;
				
			}
		}
	}

	return intersect;
}

Vector Ray::Phong(Scene &scene, Vector hitPoint, SceneObject *hitObject, int &index, int &triangleId, int rebounds)
{
	// Calculamos el vector normal y lookAt
	Vector N;
	Vector lookAtVec = (scene.GetCamera().GetPosition() - hitPoint).Normalize();
	hitObject->setValues(hitPoint, direction, triangleId, N);

	// Definimos el color de fondo y el de ambiente
	Vector bgColor = scene.GetBackground().color;
	Vector ambColor = scene.GetBackground().ambientLight;

	// Variables necesarias para el c�lculo de Phong
	Vector hitColor;
	Vector diffuseColor;
	Vector specularColor;
	Vector transparency;
	Vector reflective;
	Vector refractive;
	float shininessColor;

	// Seg�n el objeto obtenemos uno o tres materiales
	if (hitObject->IsSphere())
	{
		SceneSphere *sph = ((SceneSphere *)hitObject);
		SceneMaterial *sphMat = scene.GetMaterial(sph->material);

		diffuseColor = sphMat->diffuse;
		specularColor = sphMat->specular;
		shininessColor = sphMat->shininess;
		reflective = sphMat->reflective;
		transparency = sphMat->transparent;
		refractive = sphMat->refraction_index;
	}

	else if (hitObject->IsTriangle() || hitObject->IsModel())
	{
		SceneTriangle *tri;
		if (hitObject->IsTriangle())
		{
			tri = ((SceneTriangle *)hitObject);
		}
		else if (hitObject->IsModel())
		{
			tri = ((SceneModel *)hitObject)->GetTriangle(triangleId);
		}

		SceneMaterial *triMat0 = scene.GetMaterial(tri->material[0]);
		SceneMaterial *triMat1 = scene.GetMaterial(tri->material[1]);
		SceneMaterial *triMat2 = scene.GetMaterial(tri->material[2]);

		// Interpolamos el color final del tri�ngulo a partir del material de los v�rtices, utilizando coordenadas baric�ntricas
		Vector barycentricCoords;
		tri->Barycentric(hitPoint, barycentricCoords);

		// Texture coordinates
		float U = tri->u[0] * barycentricCoords.z + tri->u[1] * barycentricCoords.x + tri->u[2] * barycentricCoords.y;  if (U < 0) U = 0.0f;
		float V = tri->v[0] * barycentricCoords.z + tri->v[1] * barycentricCoords.x + tri->v[2] * barycentricCoords.y;  if (V < 0) V = 0.0f;
		
		Vector diffuse0, diffuse1, diffuse2;

		if (triMat0->texture != "") diffuse0 = triMat0->GetTextureColor(U, V) / 255.0f; else diffuse0 = triMat0->diffuse;
		if (triMat1->texture != "") diffuse1 = triMat1->GetTextureColor(U, V) / 255.0f; else diffuse1 = triMat0->diffuse;
		if (triMat2->texture != "") diffuse2 = triMat2->GetTextureColor(U, V) / 255.0f; else diffuse2 = triMat0->diffuse;

		diffuseColor = diffuse0 * barycentricCoords.z + diffuse1 * barycentricCoords.y + diffuse2 * barycentricCoords.x;
		specularColor = triMat0->specular * barycentricCoords.z + triMat1->specular * barycentricCoords.y + triMat2->specular * barycentricCoords.x;
		shininessColor = triMat0->shininess * barycentricCoords.z + triMat1->shininess * barycentricCoords.y + triMat2->shininess * barycentricCoords.x;
		reflective = triMat0->reflective * barycentricCoords.z + triMat1->reflective * barycentricCoords.y + triMat2->reflective * barycentricCoords.x;
		refractive = triMat0->refraction_index * barycentricCoords.z + triMat1->refraction_index * barycentricCoords.y + triMat2->refraction_index * barycentricCoords.x;
		transparency = triMat0->transparent * barycentricCoords.z + triMat1->transparent * barycentricCoords.y + triMat2->transparent * barycentricCoords.x;
	}

	hitColor = ambColor * diffuseColor;

	// Recorremos las luces de la escena e incrementamos su contribuci�n al color final
	for (int i = 0; i < scene.GetNumLights(); ++i)
	{
		SceneLight *light = scene.GetLight(i);

		Vector lightDirection = (light->position - hitPoint).Normalize();
		float rayDistance = (light->position - hitPoint).Magnitude();
		Ray lightRay = Ray(hitPoint - direction * 1e-3f, lightDirection);
		
		// Calculamos la sombra (si un objeto lo ocluye)
		SceneObject *shadowHitObject = nullptr;
		float nearestShadow = fInfinity;
		int shadowId;
		int triangleIdAux;

		bool inShadow = lightRay.Intersects(scene, nearestShadow, shadowId, triangleIdAux, &shadowHitObject, index);

		// Si no est� en sombra tenemos en cuenta el componente especular y difuso y no solo el ambiental
		if (!inShadow)
		{
			float attenuation = 1.0f / (light->attenuationConstant +
				light->attenuationLinear*rayDistance +
				light->attenuationQuadratic*rayDistance*rayDistance);

			Vector reflectionRay = lightRay.direction - N * lightRay.direction.Dot(N) * 2;
			Vector specularComponent = clamp(specularColor * pow(reflectionRay.Dot(scene.GetCamera().position - hitPoint), shininessColor));
			Vector diffuseComponent = clamp(diffuseColor * lightRay.direction.Dot(N));
			
			hitColor += light->color * (diffuseComponent + specularComponent) * attenuation;
		}
	}

	// Si nos hemos quedado sin rebotes, devolvemos el color calculado hasta el momento
	if (rebounds == 0) return hitColor;

	// Sino, seguimos lanzando rayos de forma recursiva
	if (reflective.Magnitude() > 0)
	{
		Vector reflected = direction - N * direction.Dot(N) * 2;
		reflected = reflected.Normalize();

		Ray reflectRay(hitPoint + reflected * 1e-3f, reflected);

		Vector reflectedColor = reflectRay.Cast(scene, rebounds - 1, index);
		return reflectedColor * reflective + hitColor;
	}

	if (transparency.Magnitude() > 0)
	{
		Ray refractedRayX = Refract((origin - hitPoint).Magnitude(), N, refractive.x);
		Ray refractedRayY = Refract((origin - hitPoint).Magnitude(), N, refractive.y);
		Ray refractedRayZ = Refract((origin - hitPoint).Magnitude(), N, refractive.z);

		Vector refractedColor = (refractedRayX.Cast(scene, rebounds - 1, index), refractedRayY.Cast(scene, rebounds - 1, index), refractedRayZ.Cast(scene, rebounds - 1, index));
		return refractedColor * refractive + hitColor * Vector(1 - refractedColor.x, refractedColor.y - 1, refractedColor.z - 1);
	}
}